@extends('layout.buyers_baselayout')
@section('content')
    <div class="container">
        <div class="flash-message">
            @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                @if(Session::has('alert-' . $msg))

                    <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }}
                        <a href="#"
                           class="close"
                           data-dismiss="alert"
                           aria-label="close">&times;</a>
                    </p>
                @endif
            @endforeach
        </div> <!-- end .flash-message -->

        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default panel-table">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col col-xs-6">
                                <h3 class="panel-title">My Bids</h3>
                            </div>
                            <div class="col col-xs-6 text-right">
                                {{--<a href="{{ url('/post_product')}}">
                                    <button type="button" class="btn btn-sm btn-primary btn-create">Create
                                        New
                                    </button>
                                </a>--}}
                            </div>
                        </div>
                    </div>
                    <div class="panel-body">
                        <table class="table table-striped table-bordered table-list">
                            <thead>
                            <tr>
                                <th>Sl</th>
                                <th>Image</th>
                                <th>Product Name</th>
                                <th>Biding Amount</th>
                                <th>Total Bid</th>
                                <th>Max Bid</th>
                                <th>Bid Endtime</th>
                                <th><em class="fa fa-cog"></em></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $i = 0; ?>
                            @foreach($bids as $bid)
                                @php
                                    $product = $bid->product;

                                    $productBids = $product->Bid()->get()->all();
                                    $count = count($productBids);
                                    $max = 0;
                                    foreach ($productBids as $pbid){
                                     if($pbid->price>$max){
                                       $max = $pbid->price;
                                     }
                                    }
                                @endphp

                                <tr>
                                    <td class="hidden-xs">{{$i}}</td>
                                    <td>{{--<a href="{{url('buyer/product_desc/'.$product->id)}}">--}}
                                        <img width="70px" height="70px" src="{{$product->image1}}"
                                             alt="{{$product->image1}}">
                                        {{-- </a>--}}
                                    </td>
                                    <td>{{$product->name}}</td>
                                    <td>{{$product->price}}</td>
                                    <td>{{$count}}</td>
                                    <td>{{$max}}</td>
                                    <td>{{$product->end_time}}</td>

                                    <td>
                                        <a class="btn btn-danger delete-court-button"
                                           role="button"
                                           data-action-link="/placebid/{{$product->id}}"
                                           data-toggle="modal"
                                           data-target="#modal-delete">Edit</a>
                                    </td>

                                </tr>
                                <?php $i++; ?>
                            @endforeach
                            </tbody>
                        </table>

                    </div>
                    <div class="panel-footer">
                        <div class="row">
                            <div class="col col-xs-4">Page 1 of 5
                            </div>
                            <div class="col col-xs-8">
                                <ul class="pagination hidden-xs pull-right">
                                    <li><a href="#">1</a></li>
                                    <li><a href="#">2</a></li>
                                    <li><a href="#">3</a></li>
                                    <li><a href="#">4</a></li>
                                    <li><a href="#">5</a></li>
                                </ul>
                                <ul class="pagination visible-xs pull-right">
                                    <li><a href="#">«</a></li>
                                    <li><a href="#">»</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">

                <form id="delete-court-form" method="POST" action="">

                    {{csrf_field()}}

                    <div class="modal-header text-center">
                        <h4 class="modal-title w-100 font-weight-bold">Place Bid</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <div class="modal-body mx-3">
                        <div class="text-center md-form mb-5 ">


                            <div class="form-inline">
                                <label for="price">Price(BDT):</label>
                                <input type="number" name="price" id="price">
                            </div>

                        </div>
                    </div>

                    <div class="modal-footer d-flex justify-content-center">
                        <input type="submit" value="Bid">
                    </div>


                </form>

            </div>

        </div>

        <script>
            $('.delete-court-button').on('click', function () {
                $('#delete-court-form').attr('action', $(this).data('action-link'));
            });
        </script>
    </div>

@endsection