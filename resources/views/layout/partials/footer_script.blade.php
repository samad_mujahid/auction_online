<!-- ============================================================== -->
<!-- All Jquery -->
<!-- ============================================================== -->
<script src="{!!asset('theme/assets/plugins/jquery/jquery.min.js')!!}"></script>
<!-- Bootstrap tether Core JavaScript -->
<script src="{!!asset('theme/assets/plugins/bootstrap/js/tether.min.js')!!}"></script>
<script src="{!!asset('theme/assets/plugins/bootstrap/js/bootstrap.min.js')!!}"></script>
<!-- slimscrollbar scrollbar JavaScript -->
<script src="{!!asset('theme/js/jquery.slimscroll.js')!!}"></script>
<!--Wave Effects -->
<script src="{!!asset('theme/js/waves.js')!!}"></script>
<!--Menu sidebar -->
<script src="{!!asset('theme/js/sidebarmenu.js')!!}"></script>
<!--stickey kit -->
<script src="{!!asset('theme/assets/plugins/sticky-kit-master/dist/sticky-kit.min.js')!!}"></script>
<!--Custom JavaScript -->
<script src="{!!asset('theme/js/custom.min.js')!!}"></script>
<!-- ============================================================== -->
<!-- This page plugins -->
<!-- ============================================================== -->
<!-- Flot Charts JavaScript -->
<script src="{!!asset('theme/assets/plugins/flot/jquery.flot.js')!!}"></script>
<script src="{!!asset('theme/assets/plugins/flot.tooltip/js/jquery.flot.tooltip.min.js')!!}"></script>
<script src="{!!asset('theme/js/flot-data.js')!!}"></script>
<!-- ============================================================== -->
<!-- Style switcher -->
<!-- ============================================================== -->
<script src="{!!asset('theme/assets/plugins/styleswitcher/jQuery.style.switcher.js')!!}"></script>