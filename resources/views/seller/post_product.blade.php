@extends('layout.sellers_baselayout')

@section('content')
    <div class="flash-message">
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
            @if(Session::has('alert-' . $msg))

                <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#"
                                                                                         class="close"
                                                                                         data-dismiss="alert"
                                                                                         aria-label="close">&times;</a>
                </p>
            @endif
        @endforeach
    </div> <!-- end .flash-message -->
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-register mx-auto mt-5">
                    <div align="center" class="card-header">Post a product</div>
                    <div class="card-body">
                        <form class="form-horizontal" method="POST" action="{{ route('store_product') }}"
                              enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label for="name">Product's Name</label>
                                <input class="form-control" id="name" name="name" value="{{ old('name') }}"
                                       type="text"
                                       aria-describedby="nameHelp">
                                @if ($errors->has('name'))
                                    <span class="help-block">
                                                    <strong>{{ $errors->first('name') }}</strong>
                                                </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <div class="form-inline{{ $errors->has('name') ? ' has-error' : '' }}">
                                    <div class="col-md-3">
                                        <label for="category">Category</label>
                                        <select class="form-control" id="category" name="category"
                                                type="text"
                                                aria-describedby="userType">
                                            <option value="Electronics" selected>Electronics</option>
                                            <option value="Entertainment">Media/Entertainment</option>
                                            <option value="Photography">Photography</option>
                                            <option value="Consultancy">Consultancy</option>
                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                        <label for="price">Price(BDT)</label>
                                        <input class="form-control" id="price" name="price" price="price"
                                               value="{{ old('price') }}"
                                               type="text" aria-describedby="priceHelp">
                                        @if ($errors->has('price'))
                                            <span class="help-block">
                                                            <strong>{{ $errors->first('price') }}</strong>
                                                        </span>
                                        @endif
                                    </div>
                                    <div class="col-md-3">
                                        <input type="date" name="date">
                                    </div>
                                    <div class="col-md-3">
                                        <input type="time" name="time">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="description">Product's Description</label>
                                <textarea class="form-control rounded-0"
                                          id="description" name="description"
                                          value="{{ old('description') }}"
                                          type="text" rows="3">
                                            </textarea>
                                @if ($errors->has('description'))
                                    <span class="help-block">
                                                        <strong>{{ $errors->first('description') }}</strong>
                                                    </span>
                                @endif
                            </div>

                            <div class="container-fluid">
                                <input type="file" name="image1" accept="image/*">
                            </div>
                            <button class="btn btn-primary btn-block">Submit</button>
                        </form>
                    </div>
                </div>
                <div class="text-center">
                    <br><br>
                    <br><br>
                </div>
            </div>

        </div>
    </div>
    </div>

@endsection