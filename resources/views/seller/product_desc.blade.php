@extends('layout.sellers_baselayout')
@section('content')
    <div class="content">
        <div class="card">
            <div class="row">
                <div class="col-md-2">

                </div>
                <div class="col-md-8">

                    {{--@php
                        $bids = \App\Product::find($product->id)->Bid()->get()->all();
                        $max = 0;
                        foreach ($bids as $bid){
                         if($bid->price>$max){
                           $max = $bid->price;
                         }
                        }
                    @endphp--}}

                    <h3><p class="text-center">Product Bid</p></h3>
                    <img src="{{$product->image1}}" > {{--class="center-block" alt="..." width="294" height="336"--}}
                    <br>
                    <div class="row">
                        <div class="col-md-6">
                            <p class="font-weight-light">Product Name</p>
                            <p class="font-weight-light">Product Description</p>
                            <p class="font-weight-light">Category</p>
                            <p class="font-weight-light">Minimum Bid</p>
                            <p class="font-weight-light">Bid Endtime</p>
                            <p class="font-weight-light">Total Bid Count:</p>
                            <p class="font-weight-light">Max Bid:</p>
                        </div>
                        <div class="col-md-6">
                            <p>{{$product->name}}</p>
                            <p>{{$product->description}}</p>
                            <p>{{$product->category}}</p>
                            <p>{{$product->price}}</p>
                            <p>{{$product->end_time}}</p>
                            {{--<p><a href="{{url('/see_bidders/'.$product->id)}}">{{count($bids)}}</a></p>
                            <p>{{$max}}</p>--}}

                        </div>
                    </div>
                </div>
                <div class="col-md-2">

                </div>
            </div>
        </div>
    </div>
    </div>
    </div>
@endsection

