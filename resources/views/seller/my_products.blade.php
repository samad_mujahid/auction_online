@extends('layout.sellers_baselayout')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default panel-table">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col col-xs-6">
                                <h3 class="panel-title">My Products</h3>
                            </div>
                            <div class="col col-xs-6 text-right">
                                <a href="{{ url('/post_product')}}">
                                    <button type="button" class="btn btn-sm btn-primary btn-create">Create
                                        New
                                    </button>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="panel-body">
                        <table class="table table-striped table-bordered table-list">
                            <thead>
                            <tr>
                                <th>Product Id</th>
                                <th>Image</th>
                                <th>Category</th>
                                <th>Product Name</th>
                                <th>Minimum Bid</th>
                                <th>Bid Endtime</th>
                                <th><em class="fa fa-cog"></em></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($products as $product)
                                <tr>
                                    <td class="hidden-xs">{{$product->id}}</td>
                                    <td><a href="{{url('seller/product_desc/'.$product->id)}}"><img width="70px" height="70px" src="{{$product->image1}}"
                                                                                                    alt="{{$product->image1}}"></a></td>
                                    <td>{{$product->category}}</td>
                                    <td>{{$product->name}}</td>
                                    <td>{{$product->price}}</td>
                                    <td>{{$product->end_time}}</td>
                                    <td>
                                        <a href="{{url('/edit_product/'.$product->id)}}"
                                           class="btn btn-md"><em class="fa fa-pencil"></em></a>
                                        <a href="{{url('/delete_product/'.$product->id)}}"
                                           class="btn btn-md"><em class="fa fa-trash"></em></a>
                                    </td>
                                </tr>
                            @endforeach
                            {{-- {{ $products->links() }}--}}
                            </tbody>
                        </table>

                    </div>
                    <div class="panel-footer">
                        {{--<div class="row">
                            <div class="col col-xs-4">Page 1 of 5
                            </div>
                            <div class="col col-xs-8">
                                <ul class="pagination hidden-xs pull-right">
                                    <li><a href="#">1</a></li>
                                    <li><a href="#">2</a></li>
                                    <li><a href="#">3</a></li>
                                    <li><a href="#">4</a></li>
                                    <li><a href="#">5</a></li>
                                </ul>
                                <ul class="pagination visible-xs pull-right">
                                    <li><a href="#">«</a></li>
                                    <li><a href="#">»</a></li>
                                </ul>
                            </div>--}}
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endsection