<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Bootstrap core CSS -->
    <link href= "{!!asset('theme/assets/plugins/bootstrap/css/bootstrap.min.css') !!}" rel="stylesheet">

    <!--to use jquery or ajax -->
    <script src=" {!!asset('theme/assets/plugins/jquery/jquery.min.js')!!} " }></script>
    <script src="{!!asset('theme/assets/plugins/bootstrap/js/bootstrap.bundle.js')!!}" }></script>
    <script src="{!!asset('theme/assets/plugins/bootstrap/js/bootstrap.js')!!}" }></script>

    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet" />
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300|Material+Icons' rel='stylesheet' type='text/css'>
</head>

<body class="bg-dark">
<div class="container">
    <div class="card card-register mx-auto mt-5">
        <div class="card-header">Register an Account</div>
        <div class="card-body">

            <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                {{ csrf_field() }}

                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                    <div class="form-row">
                        <div class="col-md-6">
                            <label for="name">Name</label>
                            <input class="form-control" id="name" name = "name"  value="{{ old('name') }}" type="text" aria-describedby="nameHelp"
                                   placeholder="Enter name">
                            @if ($errors->has('email'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                            @endif
                        </div>
                        <div class="col-md-6">
                            <label for="user_type">User Type</label>
                            <select class="form-control" id="user_type" name ="user_type" type="text"
                                    aria-describedby="userType" placeholder="User Type?">
                                <option value="buyer">Buyer</option>
                                <option value="seller">Seller</option>
                            </select>

                        </div>
                    </div>
                </div>

                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                    <label for="email">Email address</label>
                    <input class="form-control" id="email" name = "email" value="{{ old('email') }}" type="email" aria-describedby="emailHelp"
                           placeholder="Enter email">
                    @if ($errors->has('email'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                    @endif
                </div>

                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                    <label for="phone">Phone</label>


                    <input id="phone"   name="phone" type="text"  placeholder="Enter Phone Number" class="form-control" value="{{ old('phone') }}" >

                    @if ($errors->has('phone'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                    @endif

                </div>

                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                    <div class="form-row">
                        <div class="col-md-6">
                            <label for="password">Password</label>
                            <input class="form-control"  name="password" id="password" value="{{ old('password') }}" type="password"
                                   placeholder="Password">
                            @if ($errors->has('password'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                            @endif
                        </div>
                        <div class="col-md-6">
                            <label for="confirm_pasword">Confirm password</label>
                            <input class="form-control"  name="password_confirmation" id="password_confirmation" type="password"
                                   placeholder="Confirm password">

                        </div>
                    </div>
                </div>
                <button class="btn btn-primary btn-block">Register</button>
            </form>
            <div class="text-center">
                <a class="d-block small mt-3" href="{{route('login')}}">Login Page</a>
                <a class="d-block small" href="forgot-password.html">Forgot Password?</a>
            </div>
        </div>
    </div>
</div>


{{--
<script src=" {!!asset('admin/vendor/jquery/jquery.min.js')!!} " }></script>
<script src=" {!!asset('admin/vendor/bootstrap/js/bootstrap.bundle.min.js')!!} " }></script>
<script src=" {!!asset('admin/vendor/jquery-easing/jquery.easing.min.js')!!} " }></script>
--}}

</body>

</html>