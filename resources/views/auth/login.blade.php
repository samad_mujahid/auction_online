<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container-fluid">
    <div class="flash-message">
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
            @if(Session::has('alert-' . $msg))
                <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#"
                                                                                         class="close"
                                                                                         data-dismiss="alert"
                                                                                         aria-label="close">&times;</a>
                </p>
            @endif
        @endforeach
    </div> <!-- end .flash-message -->
    <nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="#">Online Auction</a>
            </div>
            <ul class="nav navbar-nav">
                <li class="active"><a href="#">Home</a></li>
                <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Page 1 <span
                                class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="#">Page 1-1</a></li>
                        <li><a href="#">Page 1-2</a></li>
                        <li><a href="#">Page 1-3</a></li>
                    </ul>
                </li>
                <li><a href="#">Page 2</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><a class="nav-link" href="{{route('register')}}"><span class="glyphicon glyphicon-user"></span> Sign
                        Up</a></li>
                <li><a class="nav-link" href="#loginModal" data-toggle="modal" data-target="#loginModal"><span
                                class="glyphicon glyphicon-log-in"></span> Login</a></li>
            </ul>
        </div>
    </nav>
    <!-- Page Content -->
    <!-- Heading Row -->
    <div class="row my-4">
        <div class="col-lg-8">
            <img class="img-fluid rounded" src="http://placehold.it/900x400" alt="">
        </div>
        <!-- /.col-lg-8 -->
        <div class="col-lg-4">
            <h1>Business Name or Tagline</h1>
            <p>This is a template that is great for small businesses. It doesn't have too much fancy flare to it,
                but it makes a great use of the standard Bootstrap core components. Feel free to use this template
                for any project you want!</p>
            <a class="btn btn-primary btn-lg" href="#">Call to Action!</a>
        </div>
        <!-- /.col-md-4 -->
    </div>
    <!-- /.row -->

    <!-- Call to Action Well -->
    <div class="card text-white bg-secondary my-4 text-center">
        <div class="card-body">
            <p class="text-white m-0">This call to action card is a great place to showcase some important
                information or display a clever tagline!</p>
        </div>
    </div>

    <!-- Content Row -->
    <div class="row">
        <div class="col-md-4 mb-4">
            <div class="card h-100">
                <div class="card-body">
                    <h2 class="card-title">Card One</h2>
                    <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem magni quas ex
                        numquam, maxime minus quam molestias corporis quod, ea minima accusamus.</p>
                </div>
                <div class="card-footer">
                    <a href="#" class="btn btn-primary">More Info</a>
                </div>
            </div>
        </div>
        <!-- /.col-md-4 -->
        <div class="col-md-4 mb-4">
            <div class="card h-100">
                <div class="card-body">
                    <h2 class="card-title">Card Two</h2>
                    <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod tenetur ex
                        natus at dolorem enim! Nesciunt pariatur voluptatem sunt quam eaque, vel, non in id dolore
                        voluptates quos eligendi labore.</p>
                </div>
                <div class="card-footer">
                    <a href="#" class="btn btn-primary">More Info</a>
                </div>
            </div>
        </div>
        <!-- /.col-md-4 -->
        <div class="col-md-4 mb-4">
            <div class="card h-100">
                <div class="card-body">
                    <h2 class="card-title">Card Three</h2>
                    <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem magni quas ex
                        numquam, maxime minus quam molestias corporis quod, ea minima accusamus.</p>
                </div>
                <div class="card-footer">
                    <a href="#" class="btn btn-primary">More Info</a>
                </div>
            </div>
        </div>
        <!-- /.col-md-4 -->
    </div>
    <!-- /.row -->
    <!-- /.container -->
    @if(isset($_GET['email_error'])||isset($_GET['password_error']))
        <script>
            $(function () {
                $('#loginModal').modal('show');
            });
        </script>
    @endif
    <div class="modal fade" id="loginModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Log-in</h4>
                </div>
                <div class="modal-body">

                    <form class="navbar-form navbar-right" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}

                        <div class="form-group">

                            <div>
                                <label for="email">Email</label>
                                <input id="email" type="email" class="form-control" name="email"
                                       value="{{ old('email') }}"
                                       required autofocus>

                                @if (isset($_GET['email_error']))
                                    <span class="has-error">
                                        <strong>{{ $_GET['email_error'] }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ isset($_GET['password_error']) ? ' has-error' : '' }}">

                            <div>
                                <label for="password">Password</label>
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if (isset($_GET['password_error']))
                                    <span class="has-error">
                                        <strong>{{ $_GET['password_error'] }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="">
                                <button type="submit" class="btn btn-primary">
                                    Login
                                </button>
                            </div>
                        </div>

                    </form>
                    <div class="form-group">
                        <div class="">
                            <a class="btn btn-link" href="{{ route('password.request') }}">
                                <strong>Forgot Your Password? </strong>
                            </a>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button class="btn btn-info" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <script>

        /* $(function () {

             $('#loginModal').on('hide.bs.modal', function (e) {
               // alert('time to show modal');
                 $('#loginModal').modal('show')
             })
         });
     */
        $(document).ready(function () {



            // process the form
            $('form').submit(function (event) {

                event.preventDefault();

                console.log('Sending request to ' + $(this).attr('action') + ' with data: ' + $(this).serialize());

                // get the form data
                // there are many ways to get this data using jQuery (you can use the class or id also)
                var formData = {
                    'email': $('input[name=email]').val(),
                    'password': $('input[name=password]').val(),

                };

                // process the form
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('input[name="_token"]').attr('value')
                    },
                    type: 'POST', // define the type of HTTP verb we want to use (POST for our form)
                    url: $(this).attr('action'), // the url where we want to POST
                    data: $(this).serialize(), // our data object

                    // what type of data do we expect back from the server

                    success: function (response) {
                        console.log("Success");
                        $(this).modal('hide');
                        window.location = '/home';

                    },
                    error: function (jqXHR) {
                        var response = $.parseJSON(jqXHR.responseText);
                        console.log("Error");
                        if (response.email) {

                            //$('#loginModal').modal('hide')
                            window.location = '?email_error=' + response.email;

                        }

                        if (response.password) {
                            window.location = '?password_error=' + response.password;
                        }
                    }
                });


            });
        })


        /* $(document).ready(function () {

             // process the form
             $('form').submit(function (event) {

                 event.preventDefault();

                 console.log('Sending request to ' + $(this).attr('action') + ' with data: ' + $(this).serialize());

                 // get the form data
                 // there are many ways to get this data using jQuery (you can use the class or id also)
                 var formData = {
                     'email': $('input[name=email]').val(),
                     'password': $('input[name=password]').val(),

                 };

                 // process the form
                 $.ajax({
                     headers: {
                         'X-CSRF-TOKEN': $('input[name="_token"]').attr('value')
                     },
                     type: 'POST', // define the type of HTTP verb we want to use (POST for our form)
                     url: $(this).attr('action'), // the url where we want to POST
                     data:$(this).serialize() // our data object

                     // what type of data do we expect back from the server

                 })
                 // using the done promise callback
                     .done(function (data) {

                         // log data to the console so we can see
                         console.log(data);

                         // here we will handle errors and validation messages
                         if (!data.success) {

                             // handle errors for email ---------------
                             /!*if (data.errors.email) {
                                 $('#email-group').addClass('has-error'); // add the error class to show red input
                                 $('#email-group').append('<div class="help-block">' + data.errors.email + '</div>'); // add the actual error message under our input
                             }
     *!/

                         } else {
                             // ALL GOOD! just show the success message!
                             $('form').append('<div class="alert alert-success">' + data.message + '</div>');

                             // usually after form submission, you'll want to redirect
                             // window.location = '/thank-you'; // redirect a user to another page
                             alert('success'); // for now we'll just alert the user
                         }
                     })
                     .fail( function(xhr, textStatus,error) {
                         console.log("Error");
                         console.log(error);
                     });



             });

         });
     */
    </script>

</div>

</body>
</html>
