<?php

namespace App\Http\Controllers;
use Auth;

class HomeController extends Controller
{
    public function index()
    {
        $user = Auth::user();

        if($user->user_type == 'buyer'){
            return redirect()->route('buyers_home');
        }
        else if($user->user_type == 'seller') {
            return redirect()->route('sellers_home');
        }
        else if($user->user_type == 'admin') {
            return  redirect()->route('admins_home');
        }
        else {
            echo 'User type not matched with any predefined';
        }
    }
}
