<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;


class SellerController extends Controller
{
    public function index()
    {
        $user = Auth::user();
        /*$user = user::paginate(10);*/
        return view('seller.sellers_home')->with('user', $user);

    }

}
