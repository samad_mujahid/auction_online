<?php

namespace App\Http\Controllers;

use App\product;
use Illuminate\Http\Request;
use App\Bid;
use Auth;

class BuyersController extends Controller
{
    public function index()
    {
        $user = Auth::user();
        return view('buyer.buyers_home')->with('user',$user);
    }

    public function  products()
    {
        $products = Product::all();
        return view('buyer.all_products')->with('products', $products);
    }

    public function mybids()
    {
        $user = Auth::user();
        $bids = $user->bid;
        return view('buyer.my_bids')->with('bids', $bids);
    }

    public function placebid(Request $request,$product_id)
    {
        $user = Auth::user();

        $bids = \App\Product::find($product_id)->Bid()->get()->all();
        $max = 0;
        foreach ($bids as $bid)
        {
            if($bid->price>$max){
                $max = $bid->price;
            }
        }

        if($bid->price < $max)
        {
            $request->session()->flash('alert-danger', 'Bid amount must be greater than current max: '.$max);
            return back();
        }

        $existed = $user->Bid()->where('product_id',$product_id)->get()->first();

        if($existed)
        {
            $bid = $existed;
            $bid->price = $request->price;
            $bid->save();
            $request->session()->flash('alert-success', 'Edited successfully');
            return back();
        } else {

            $bid = new Bid();
            $bid->price = $request->price;
            $bid->product_id = $product_id;
            $user->Bid()->save($bid);
        }

        $request->session()->flash('alert-success', 'Posted successfully');
        return back();
    }

}
