<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;

use App\Product;

class ProductController extends Controller
{

    public function myProduct()
    {
        $user = auth()->user();
        $products = $user->Product()->get();

        return view('seller.my_products')->with('products', $products);
    }

    public function createProduct()
    {
        $user = Auth::user();
        return view('seller.post_product')->with('user', $user);
    }

    public function product_desc ($id) {
        $product = \App\Product::find($id);
        return view('seller.product_desc')->with('product',$product);
    }

    public function storeProduct(Request $request)
    {

        $user = auth()->user();
        $product = new Product();

        /*$product->validate($request->all());*/

        $product->fill($request->all());

        if ($request->hasFile('image1')) {
            $imageName = $request->file('image1')->getClientOriginalName();
            $target = $request->file('image1')->move(public_path('product_picture'), $imageName);
            $product->image1 = 'product_picture/' . $imageName;
        }

        $user->Product()->save($product);

        $request->session()->flash('alert-success', 'Posted successfully');
        return back();

    }

    public function edit_product($id)
    {
        $products = Product::find($id);
        return view('seller.edit_product')->with('product',$products);
    }

    public function update_product(Request $request, $id)
    {

        $product = Product::find($id);

        /*$product->validate($request->all());*/

        $product->fill($request->all());

        if ($request->hasFile('image1')) {

            $image = $request->file('image1');
            $name = time() . '.' . $image->getClientOriginalExtension();
            $imageName = $request->file('image1')->getClientOriginalName();
            //return base_path() . '/public/uploads';
            $target = $request->file('image1')->move(public_path('product_picture'), $imageName);
            $product->image1 = 'product_picture/' . $imageName;
        }

        $product->save();


        $request->session()->flash('alert-success', 'Posted successfully');
        return back();

    }
    public function delete_product($id){
        $product = Product::find($id);
        $product->delete();
        return back();

    }


}
