<?php

namespace App\Http\Controllers;

use App\Admin;
use App\PendingProduct;
use App\Product;
use App\User;
use Illuminate\Http\Request;

class AdminController extends Controller
{

    public function index(){
        $products = PendingProduct::all();
        return view('admin.pending_products')->with('products',$products);
    }



    public function pending_products(){
        $products = PendingProduct::all();
        return view('admin.pending_products')->with('products',$products);
    }

    public function  products(){
        $products = Product::all();
        return view('admin.all_products')->with('products', $products);
    }

    public function approve_pending_product($id){

        $pendingProduct = PendingProduct::find($id);
        $user = User::find($pendingProduct->user_id);
        $product = new Product();
        $product->name = $pendingProduct->name;
        $product->category = $pendingProduct->category;
        $product->price = $pendingProduct->price;
        $product->description = $pendingProduct->description;
        $product->end_time = $pendingProduct->end_time;
        $product->image1 = $pendingProduct->image1;
        $product->image2 = $pendingProduct->image2;
        $product->image3 = $pendingProduct->image3;
        $product->image4 = $pendingProduct->image4;
        $product->image5 = $pendingProduct->image5;
        $user->Product()->save($product);
        $pendingProduct->delete();
        return redirect()->back();
    }

    public function delete_pending_product($id){

        $product = PendingProduct::find($id);
        if (!isset($product)) return;
        $product->delete();
        return back();
    }



    public function edit_product($id)
    {
        $product = Product::find($id);
        if (!isset($product)) return;
        return view('admin.edit_product')->with('product', $product);
    }

    public function update_product(Request $request, $id)
    {

        $product = Product::find($id);

        if (!isset($product)) {
            $request->session()->flash('alert-danger', 'Product not found in List, May be its not aprroved by admin');
            return back();
        }

        $product->validate($request->all());

        $product->fill($request->all());

        if ($request->hasFile('image1')) {

            $image = $request->file('image1');
            $name = time() . '.' . $image->getClientOriginalExtension();
            $imageName = $request->file('image1')->getClientOriginalName();
            //return base_path() . '/public/uploads';
            $target = $request->file('image1')->move(public_path('product_picture'), $imageName);
            $product->image1 = 'product_picture/' . $imageName;
        }

        $product->save();

        $request->session()->flash('alert-success', 'Posted successfully');

        return back();

    }

    public function delete_product($id)
    {
        $product = Product::find($id);
        if (!isset($product)) return;
        $product->delete();
    }



}
