<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class product extends Model
{
    protected $guarded = ['created_at','updated_at'];

    public function User(){
        return $this->belongsTo(User::class);
    }

    public function Bid(){
        return $this->hasMany(Bid::class);
    }
}
