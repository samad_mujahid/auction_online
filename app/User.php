<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','user_type', 'email','phone', 'password',
    ];

    public function Product(){

        return $this->hasMany(product::Class);

    }
    public function BidderedProducts(){

        return $this->belongsTo(Product::class,Bid::class);

    }

    public function Bid(){

        return $this->hasMany(Bid::Class);

    }
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}
