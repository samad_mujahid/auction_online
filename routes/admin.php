<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => ['webadmin']], function () {

    Route::post('logout', 'AdminAuth\LoginController@logout')->name('webadmin_logout');

    Route::get('home', 'AdminController@index')->name('webadmin_home');
    Route::get('/', 'AdminController@index')->name('webadmin_home');

    Route::get('approve_pending_product/{id}', 'AdminController@approve_pending_product');
    Route::get('delete_pending_product/{id}', 'AdminController@delete_pending_product');

    Route::get('/categories', 'CategoryController@index');
    Route::get('/add_category', 'CategoryController@create');
    Route::post('/category', 'CategoryController@store');
    Route::get('/delete_category/{id}', 'CategoryController@delete');

    Route::get('/products', 'AdminController@products')->name('products');
    Route::get('/pending_products', 'AdminController@pending_products')->name('products');

    Route::get('/edit_product/{id}', 'AdminController@edit_product');
    Route::post('/update_product/{id}', 'AdminController@update_product')->name('update_product');
    Route::get('/delete_product/{id}', 'AdminController@delete_product');
});

Route::group(['middleware' => ['web']], function () {

    Route::post('/register','AdminAuth\RegisterController@create');
    Route::post('login', 'AdminAuth\LoginController@login')->name('webadmin_login');
    Route::get('login', 'AdminAuth\LoginController@index');
});






