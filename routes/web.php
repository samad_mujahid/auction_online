<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();


Route::group(['middleware' => ['auth']], function () {




    Route::get('/', 'HomeController@index')->name('home');
    Route::get('/home', 'HomeController@index')->name('home');

    Route::get('/buyers_home', 'BuyersController@index')->name('buyers_home');
    Route::get('/all_products', 'BuyersController@products')->name('all_products');

    Route::get('/mybids','BuyersController@mybids')->name('mybids');
    Route::post('/placebid/{id}', 'BuyersController@placebid')->name('placebid');

    Route::get('/post_product', 'ProductController@createProduct');
    Route::post('/post_product', 'ProductController@storeProduct')->name('store_product');

    Route::get('/myproducts', 'ProductController@myProduct')->name('myproducts');
    Route::get('/edit_product/{id}', 'ProductController@edit_product');
    Route::post('/update_product/{id}', 'ProductController@update_product')->name('update_product');
    Route::get('/delete_product/{id}', 'ProductController@delete_product');
    Route::get('/seller/product_desc/{id}', 'ProductController@product_desc');

    Route::get('/sellers_home', 'SellerController@index')->name('sellers_home');

});

Route::get('/date_test', function () {
    return view('date_test');
});


Route::get('/monster', function () {
    return view('monster');
});



Route::get('/mainpage', function () {
    return view('mainpage');
});

